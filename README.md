## 仓库介绍

基于 uniapp 默认模板创建的 uniapp 项目, 封装一些开发中常用的功能模块

## 工具类封装

### uni.$cache
---

本地数据缓存模块封装分为三部分: `config`、`actions`、`export default`

+ config 缓存配置
+ actions 内部操作缓存的逻辑代码
+ export default 对外暴露的方法

封装思想

+ 缓存数据的 key 和有效时间在 config 里面集中管理
+ 对外不能直接操作缓存数据, 而是通过封装的方法操作缓存
+ 通过打印 `uni.$cache` 查看缓存模块对外暴露的方法,直接调用,无需指定配置key

```js
uni.$cache.setToken(data)
uni.$cache.getToken()
uni.$cache.delToken()
```

### uni.$tools.toast()
---

语法格式:

```js
uni.$tools.toast(消息提示, 页面跳转)
```

消息提示框:

```js
// 最简单的消息提示框
uni.$tools.toast('操作成功')

// 自定义消息提示框的参数
uni.$tools.toast({
	mask: true,
	icon: 'success',
	duration: 3000,
	title: '提交成功',
})
```

消息提示后进行页面跳转:

```js
// 第二个参数是字符串,并且以~波浪号开头【推荐用法】
// 这种情况下会自动从tools.js中的pages变量中读取页面跳转
uni.$tools.toast('操作成功', '~login')

// 可以将页面路径作为第二个参数直接传入,使用默认的跳转方式跳转
uni.$tools.toast('操作成功', '/pages/login/login')

// 自定义跳转方式,第二个参数可以传入一个对象
uni.$tools.toast('操作成功', { url: '/pages/login/login', type: 2 })

// 第二个参数也支持传入一个函数,可以更加灵活的定义消息提示结束的操作
uni.$tools.toast('操作成功', () => {
	// 消息提示消失后自动执行该函数
})
```

消息提示后返回上级页面并调用指定方法:

```js
// 返回上一级页面并调用上一级页面的 getUserInfo 方法
uni.$tools.toast('修改成功', '^getUserInfo')
```

### uni.$tools.loading()
---

显示加载动画

```js
// 最简单用法
uni.$tools.loading('正在上传')

// 自定义参数
uni.$tools.loading({
	mask: false,
	title: '正在上传'
})
```

### uni.$tools.hideLoading()
---

语法格式:

```js
uni.$tools.hideLoading(消息提示, 页面跳转)
```

`hideLoading()` 和 `toast()` 参数完全一致, `hideLoading()` 方法代码:

```js
hideLoading(options, navigate) {
	uni.hideLoading()
	this.toast(options, navigate)
}
```

loading() 和 hideLoading() 使用示例:

```js
// 显示加载动画
uni.$tools.loading('正在上传')
// 接口响应成功后显示消息提示并进行页面跳转
setTimeout(() => {
	uni.$tools.hideLoading('上传成功', '~userProfile')
}, 1500)
```

### uni.$tools.timestamp()
---

获取当前时间戳, 单位:秒

```js
uni.$tools.timestamp()
```

### 代码提示(项目代码块)
---

因为封装的工具类方法会频繁使用,所以在.hbuilderx目录下添加了自定义项目代码块

基本用法: `$t.方法名` + `tab键`, 示例如下:

```js
$t.toast
$t.route
$t.loading
$t.hideLoading
$t.timestamp
```