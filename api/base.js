module.exports = api => {
	const data = {}
	api.forEach(item => {
		const method = item.method.toLowerCase()
		return data[item.name] = params => {
			if (method === 'get') {
				params = { params }
			}
			return uni.$u.http[method](item.url, params)
		}
	})
	return { ...data }
}