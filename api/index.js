// 接口列表
const api =  [
	{ name: 'myTest', url: '/index/test', method: 'get' },
	{ name: 'indexData', url: '/index/upload', method: 'post' },
]
// 导出 api
module.exports = require('./base')(api);