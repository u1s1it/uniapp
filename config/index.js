export default {
	// 开发环境接口域名前缀
	develop_domain: "http://tp6.cy",
	// 正式环境接口域名前缀
	product_domain: "https://www.itqaq.com",
	// 请求超时时间
	request_timeout: 60000,
	// 请求数据传输方式
	request_dataType: 'json',
	// 与后端约定的token字段名
	token_name: 'token',
}
