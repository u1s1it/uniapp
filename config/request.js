import config from './index';

const {
	develop_domain,
	product_domain,
	request_timeout,
	request_dataType
} = config

const request = {
	// 开发环境
	develop_domain,
	// 生产环境
	product_domain,
	dataType: request_dataType,
	// #ifndef MP-ALIPAY
	responseType: 'text',
	// #endif
	// 注：如果局部custom与全局custom有同名属性，则后面的属性会覆盖前面的属性，相当于Object.assign(全局，局部)
	custom: {}, // 全局自定义参数默认值
	// #ifdef H5 || APP-PLUS || MP-ALIPAY || MP-WEIXIN
	timeout: request_timeout,
	// #endif
	// #ifdef APP-PLUS
	sslVerify: true,
	// #endif
	// #ifdef H5
	// 跨域请求时是否携带凭证（cookies）仅H5支持（HBuilderX 2.6.15+）
	withCredentials: false,
	// #endif
	// #ifdef APP-PLUS
	firstIpv4: false, // DNS解析时优先使用ipv4 仅 App-Android 支持 (HBuilderX 2.8.0+)
	// #endif
	// 局部优先级高于全局，返回当前请求的task,options。请勿在此处修改options。非必填
	// getTask: (task, options) => {
	// 相当于设置了请求超时时间500ms
	//   setTimeout(() => {
	//     task.abort()
	//   }, 500)
	// },
	// 全局自定义验证器。参数为statusCode 且必存在，不用判断空情况。
	validateStatus: (statusCode) => { // statusCode 必存在。此处示例为全局默认配置
		return statusCode >= 200 && statusCode < 300
	}
}

module.exports = vm => {
	// 全局配置 uView 网络请求
	uni.$u.http.setConfig(config => {
		// 根据环境自动切换使用的接口域名
		if (process.env.NODE_ENV === "development") {
			// 开发环境接口域名
			config.baseURL = request.develop_domain
		} else if (process.env.NODE_ENV === "production") {
			// 打包后使用的接口域名
			config.baseURL = request.product_domain
		}
		return config
	})
	// 请求拦截器
	uni.$u.http.interceptors.request.use(config => { // 可使用async await 做异步操作
		// 初始化请求拦截器时，会执行此方法，此时data为undefined，赋予默认{}
		config.data = config.data || {}

		// 示例一、根据custom参数中配置的是否需要token，添加对应的请求头
		// if (config?.custom?.auth) {
		// 	// 可以在此通过vm引用vuex中的变量，具体值在vm.$store.state中
		// 	config.header.token = vm.$store.state.userInfo.token
		// }
		// 示例二、判断是有token,有token就添加到请求头中
		return config
	}, config => { // 可使用async await 做异步操作
		return Promise.reject(config)
	})
	// 响应拦截器
	uni.$u.http.interceptors.response.use(response => {
		/* 对响应成功做点什么 可使用async await 做异步操作*/
		const {
			data: res,
			statusCode
		} = response
		// 请求成功,但后台的业务状态码异常
		if (statusCode == 200 && res.code !== 200) {
			uni.$u.toast(res.msg)
			return Promise.reject(res)
		}
		return statusCode === 200 ? res : {}
	}, response => {
		// 对响应错误做点什么(statusCode !== 200)
		uni.$u.toast('请求响应失败')
		return Promise.reject(response)
	})
}
