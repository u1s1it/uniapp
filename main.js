import App from "./App"

// #ifndef VUE3
import Vue from "vue"
Vue.config.productionTip = false
App.mpType = "app"

// 导入封装的方法
require("./utils")

// 导入 uView JS库
import uView from '@/uni_modules/uview-ui'
Vue.use(uView)

// 注册全局过滤器
import filters from "./filters"
Object.keys(filters).forEach(key => {
	Vue.filter(key, filters[key])
})

// 导入 Vuex 状态管理 Store
import store from "./store"
uni.$store = store

const app = new Vue({
	...App,
	store,
})

// 引入请求封装,将app参数传递到配置中
require('./config/request')(app)

app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from "vue"
export function createApp() {
	const app = createSSRApp(App)
	return {
		app,
	}
}
// #endif
