// 1.导入 vue 和 vuex
import Vue from "vue"
import Vuex from "vuex"
// 2.安装 Vuex
Vue.use(Vuex)
// 3.模块分离
const files = require.context("./modules", false, /\.js$/)
const modules = {
	state: {},
	getters: {},
	mutations: {},
	actions: {},
}
files.keys().forEach(key => {
	Object.keys(modules).forEach(item => {
		Object.assign(modules[item], files(key).default[item])
	})
})
// 4.实例化 store 对象
const store = new Vuex.Store(modules)
// 5.默认导出 store 对象
export default store
