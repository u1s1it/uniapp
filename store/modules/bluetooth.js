export default {
    state: {
		systemBluetoothState: false, //系统蓝牙状态
		bleIsInit: false //蓝牙初始化状态
	},
	mutations: {
		/**
		 * 修改系统蓝牙状态
		 */
		systemBluetoothState(state, value) {
			state.systemBluetoothState = value
		},
		/**
		 * 修改蓝牙初始化状态
		 */
		bleIsInit(state, value) {
			state.bleIsInit = value
		},
	}
}