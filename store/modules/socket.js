export default {
    state: {
		isConnect: false, // socket连接状态
	},
	mutations: {
		setIsConnect(state, value) {
			state.isConnect = value
		}
	}
}