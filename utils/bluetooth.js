export default {
	/**
	 * 打开系统蓝牙
	 */
	openSystemBluetooth() {
		if (uni.$tools.platform() == 'android') {
			var main = plus.android.runtimeMainActivity();
			var BluetoothAdapter = plus.android.importClass("android.bluetooth.BluetoothAdapter");
			var BAdapter = new BluetoothAdapter.getDefaultAdapter();
			if (!BAdapter.isEnabled()) {
				BAdapter.enable();
			}
		} else {
			var UIApplication = plus.ios.import("UIApplication");
			var application2 = UIApplication.sharedApplication();
			var NSURL2 = plus.ios.import("NSURL");
			var setting2 = NSURL2.URLWithString("app-settings:");
			application2.openURL(setting2);
			plus.ios.deleteObject(setting2);
			plus.ios.deleteObject(NSURL2);
			plus.ios.deleteObject(application2);
		}
	},
	/**
	 * 初始化蓝牙模块
	 */
    initBluetooth() {
		return new Promise((resolve, reject) => {
			uni.openBluetoothAdapter({
				success (res) {
					console.log('初始化蓝牙模块成功', res)
					uni.$store.commit('bleIsInit', true)
					resolve(res)
				},
				fail (err) {
					console.log('初始化蓝牙模块失败', err)
					uni.$store.commit('bleIsInit', false)
					reject(err)
				}
			})
		})
	},
	search() {
		return new Promise((resolve, reject) => {
			if (uni.$store.state.bleIsInit) {
				uni.startBluetoothDevicesDiscovery({
				  services: [],
				  success(res) {
				    console.log(res)
				  }
				})
			}
		})
	},
	/**
	 * 搜索蓝牙设备
	 */
	searchBluetooth() {
		if (uni.$store.state.bleIsInit) {
			this.search()
		} else {
			this.initBluetooth().then(res => {
				this.searchBluetooth()
			}).catch(err => {
				// 错误信息
			})
		}
	}
}