// +----------------------------------------------------------------------
// | 本地数据缓存模块封装
// +----------------------------------------------------------------------
// | 调用方式: uni.$cache
// +----------------------------------------------------------------------

const config = {
	expire: false,
	prefix: 'liang_',
	keys: {
		token: { name: 'user_token', time: 86400 },
	}
}

const actions = {
	/**
	 * 设置本地缓存
	 * expire 有效时间(秒),0代表永久有效
	 */
	set(key, value, expire = 0) {
		let data = {}
		if (config.expire && expire > 0 ) {
			data = { expire: uni.$tools.timestamp() + expire, data: value }
		} else {
			data = { expire: 0, data: value }
		}
		uni.setStorageSync(config.prefix + key, JSON.stringify(data))
	},
	/**
	 * 获取本地缓存
	 */
	get(key) {
		// 读取本都缓存
		const string = uni.getStorageSync(config.prefix + key)
		// 缓存中没有该缓存key时返回空字符串
		if (!string) return false
		// json 数据转为对象
		const { data, expire } = JSON.parse(string)
		// 缓存过期时移除缓存
		if ( config.expire && expire > 0 && uni.$tools.timestamp() > expire) {
			this.remove(key)
			return false
		}
		return data
	},
	/**
	 * 移除本地缓存
	 */
	remove (key) {
		uni.removeStorageSync(config.prefix + key)
	},
}

export default {
	setToken(data) {
		actions.set(config.keys.token.name, data, config.keys.token.time)
	},
	getToken() {
		return actions.get(config.keys.token.name)
	},
	delToken() {
		actions.remove(config.keys.token.name)
	}
}