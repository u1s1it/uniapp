export default {
	// 建立websocket连接
    connect() {
		if(uni.$store.state.isConnect) {
			return uni.$tools.toast('当前已连接,请勿重复建立连接')
		}
		// 创建一个 WebSocket 连接
		uni.connectSocket({
			url: "ws://127.0.0.1:2345",
			method: 'POST',
			success(res) {
				// console.log('WebSocket 连接创建成功', res);
			},
			fail(res) {
				console.log('WebSocket 连接创建失败', res);
			},
		})
		// 监听WebSocket连接打开事件
		uni.onSocketOpen(function (res) {
			console.log('WebSocket连接已打开!');
			uni.$store.commit('setIsConnect', true)
		})
		// 监听WebSocket错误
		uni.onSocketError(function (res) {
			console.log('WebSocket连接打开失败，请检查!');
		})
		// 监听WebSocket关闭
		uni.onSocketClose(function (res) {
			console.log('WebSocket连接已关闭！');
			uni.$store.commit('setIsConnect', false)
		})
		// 监听WebSocket接受到服务器的消息事件
		uni.onSocketMessage(function (res) {
			console.log('收到服务器内容：' + res.data);
		})
	},
	// 关闭websocket连接
	close() {
		if(uni.$store.state.isConnect) {
			uni.closeSocket()
		}
	},
	// 发送消息
	sendMsg(msg) {
		if(!uni.$store.state.isConnect) {
			return uni.$tools.toast('请先建立WebSocket连接')
		}
		uni.sendSocketMessage({ data: msg })
	}
}