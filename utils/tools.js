// +----------------------------------------------------------------------
// | 调用方式: uni.$tools.方法名()
// +----------------------------------------------------------------------
// | toast 消息提示,支持页面跳转
// +----------------------------------------------------------------------
// | route 页面跳转
// +----------------------------------------------------------------------
// | loading 加载动画
// +----------------------------------------------------------------------
// | hideLoading 隐藏加载动画,支持页面跳转
// +----------------------------------------------------------------------
// | timestamp 获取时间戳(秒)
// +----------------------------------------------------------------------
// | isWechat 是否为微信浏览器
// +----------------------------------------------------------------------
// | platform 获取平台标识
// +----------------------------------------------------------------------

const pages = {
	// +-------------------------------------------------+
	// | 1 navigateTo 	跳转至非 tabBar 页面				 |
	// +-------------------------------------------------+
	// | 2 switchTab 	跳转至 tabBar 页面				 |
	// +-------------------------------------------------+
	// | 3 redirectTo 	关闭当前页面并跳转至非 tabBar 页面	 |
	// +-------------------------------------------------+
	// | 4 reLaunch 	关闭当前所有页面并跳转至其他页面	 |
	// +-------------------------------------------------+
	// | 5 navigateBack 关闭当前页面，返回上一页面或多级页面 |
	// +-------------------------------------------------+
	// 首页
	home: { url: '/pages/index/index', type: 2 },
	// 登录
	login: { url: '/pages/login/login', type: 4 },
	// 个人中心
	userProfile: { url: '/pages/user/profile', type: 2 },
}

export default {
	/**
	 * 消息提示,支持页面跳转
	 */
	toast(options, navigate) {
		let { icon, mask, duration, title } = options
		icon = icon || "none"
		mask = mask || true
		duration = duration || 1500
		title = typeof options === "string" ? options : title
		// 消息提示
		uni.showToast({ icon, mask, duration, title })
		// 页面跳转
		const dataType = typeof navigate
		if (["string", "object", "function"].includes(dataType)) {
			setTimeout(() => {
				switch (dataType) {
					case "string":
						if (/^~(.+)/.test(navigate)) {
							const page = pages[navigate.substr(1)]
							this.route(page.url, page.type)
						} else if(/^\^(.+)/.test(navigate)) {
							// 调用的上级页面方法
							let method = navigate.substr(1)
							// 当前页面栈的实例
							let pages = getCurrentPages()
							// 上一个页面实例对象
							let beforePage = pages[pages.length - 2]
							// 触发上一个页面中的方法
							beforePage.$vm[method]()
							// 返回上一个页面
							uni.navigateBack()
						} else {
							this.route(navigate)
						}
						break
					case "object":
						this.route(navigate.url, navigate.type)
						break
					case "function":
						navigate()
						break
				}
			}, duration)
		}
	},
	/**
	 * 页面跳转
	 */
	route(url, type = 1) {
		switch (type) {
			case 1:
				// 跳转至非 tabBar 页面
				uni.navigateTo({ url })
				break
			case 2:
				// 跳转至 tabBar 页面
				uni.switchTab({ url })
				break
			case 3:
				// 关闭当前页面并跳转至非 tabBar 页面
				uni.redirectTo({ url })
				break
			case 4:
				// 关闭当前所有页面并跳转至其他页面
				uni.reLaunch({ url })
				break
			case 5:
				// 关闭当前页面，返回上一页面或多级页面
				uni.navigateBack({ delta: parseInt(url) })
				break
		}
	},
	/**
	 * 显示加载动画
	 */
	loading(options) {
		let { title, mask = true } = options
		title = typeof options === "string" ? options : title
		uni.showLoading({ mask, title })
	},
	/**
	 * 隐藏加载动画,支持页面跳转
	 */
	hideLoading(options, navigate) {
		uni.hideLoading()
		this.toast(options, navigate)
	},
	/**
	 * 获取时间戳(秒)
	 */
	timestamp() {
		return Math.round(new Date() / 1000)
	},
	/**
	 * 是否为微信浏览器
	 */
	isWechat: function() {
		var ua = window.navigator.userAgent.toLowerCase();
		return ua.match(/micromessenger/i) == 'micromessenger'
	},
	/**
	 * 获取平台标识
	 */
	platform() {
		let platform = '';
		// #ifdef H5
		platform = this.isWechat() ? 'wxBrowser' : 'h5'
		// #endif
		// #ifdef APP-PLUS
		// ios, android
		platform = uni.getSystemInfoSync().platform;
		// #endif
		// #ifdef MP-WEIXIN
		platform = 'wxMiniProgram';
		// #endif
		return platform
	}
}
